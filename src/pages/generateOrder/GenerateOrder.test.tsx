import { render, screen } from '@testing-library/react';
import { GenerateOrder } from './GenerateOrder';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';

describe("<Home />", () => {
  const setup = () => {render(<GenerateOrder />, {wrapper: MemoryRouter})}
  it("Deve renderizar o título da página.", () => {
    setup()
    const titulo = screen.getByTestId("titulo");
    expect(titulo.textContent).toBe("Agendamento de Avaliação")
  })
}) 