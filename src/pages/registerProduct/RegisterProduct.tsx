import React from 'react'
import { Link } from 'react-router-dom'

export function RegisterProduct() {
  return (
    <div>
      <h1>CADASTRAR PRODUTO</h1>
      <Link to='/'>Back Home</Link>
      <Link to='/generateOrder'>Gerar Pedido</Link>
    </div>
  )
}
