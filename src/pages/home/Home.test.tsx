import { render, screen } from '@testing-library/react';
import { Home } from './Home';
import '@testing-library/jest-dom';
import { MemoryRouter } from 'react-router-dom';

describe("<Home />", () => {
  const setup = () => {render(<Home />, {wrapper: MemoryRouter})}
  it("Deve renderizar o título da página.", () => {
    setup()
    const titulo = screen.getByTestId("titulo");
    expect(titulo.textContent).toBe("Agendamento de Avaliação")
  })
}) 