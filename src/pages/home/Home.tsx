import { Link } from 'react-router-dom';

export function Home() {
  return (
    <div>
      <h1>DELLANCHES</h1>
      <Link to='/registerProduct'>Cadastrar Produto</Link>
      <Link to='/generateOrder'>Gerar Pedido</Link>
    </div>
  )
}
