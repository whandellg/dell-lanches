import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './pages/home';
import RegisterProduct from './pages/registerProduct';
import GenerateOrder from './pages/generateOrder';

export default function Routers() {
  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
        </Routes>
        <Routes>
          <Route path="/registerProduct" element={<RegisterProduct />} />
        </Routes>
        <Routes>
          <Route path="/generateOrder" element={<GenerateOrder />} />
        </Routes>
    </BrowserRouter>
  )
}
